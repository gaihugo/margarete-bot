# Margarete Bot

## Objetivos

- Anotar coisas
- Pesquisas no dicionario
- Lista de recomendações
- Lista de Tarefas
- Link Dos Documentos de Anotações e Recomendações
- Diversão (kiss,cancelar,tapão,salve,say,curiosidade,fatality)

## Todos

- [x] Criar bot no Discord
- [x] Mandar uma mensagem
- [x] Comando say
- [x] Comando spk
- [x] Comando clear
- [x] Radom Activity
- [ ] Coloca-la num Host
- [ ] Help
- [ ] Comando salve
- [ ] Google Docs API
- [ ] Carregar documento em um embed
- [ ] Escrever num documento
- [ ] Cancelar
- [ ] Curiosidade
- [ ] Kiss,Tapão
- [ ] Fatality

## Comandos

| Comando | Ação                                                                        |
| ------- | --------------------------------------------------------------------------- |
| ?help   | Escreve no chat a lista de comandos (Embed)                                 |
| ?say    | Escreve no chat aquilo que foi escrito ao lado do comando                   |
| ?spk    | Escreve no chat aquilo que foi escrito ao lado do comando (tts = true)      |
| ?clear  | Apaga algumas mensagens de um canal de texto                                |
| ?salve  | Escreve no chat um salve com frase aleatoria (tts = true)                   |
| ?nt     | Anota em um documento do google aquilo que foi escrito ao lado do comando   |
| ?lk     | Escreve no chat sua lista de anotações (Embed)                              |
| ?rec    | Anota na lista de recomendação aquilo que foi escrito ao lado do comando    |
| ?list   | Escreve no chat a lista de recomendações (Embed)                            |
| ?mn     | Escreve no chat o significado da palavra que foi escrito ao lado do comando |
| ?fatal  | Manda a creusa cantar imediatamente o som de fatality                       |
| ?wtf    | Escreve no chat algo aleatorio da wikipedia (tts = true)                    |
| ?kiss   | Monta um embed envia no chat com foto de beijo e menciona os beijoqueiros   |
| ?tapa   | Monta um embed envia no chat com foto de tapa e menciona os tapeados        |
| ?cancel | Manda uma foto de cancelamento da pessoa mencionada ao lado do comando      |
| ?links  | Manda o link dos documentos do google escritos (Embed)                      |
| ?td     | Adiciona a atividadade mencionada a uma lista de tarefas                    |
| ?tds    | Escreve no chat a lista de tarefas numerada (Embed)                         |
| ?dt     | Deleta da lista de tarefas a tarefa feita.Por numero                        |
